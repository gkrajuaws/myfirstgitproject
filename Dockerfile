FROM docker.repo.abc.com/abc/grp-pi-managed-images/jboss-eap-72:2.1.0-1.2.14
LABEL maintainer="gkrajuaws@gmail.com"
LABEL desciption="BANKDIsputeTransAPI"
COPY env_files  /tmp/env_files
COPY configuration/standalone.xml $JBOSS_HOME/standalone/configuration/
COPY moduleArtifacts/ $JBOSS_HOME/modules/system/layers/base/
COPY *EAR/build/libs/*.ear $JBOSS_HOME/standalone/deployments/

CMD source /tmp/env_files/${RUNTIME_ENV}_env.conf && trap "" TERM && exec $JBOSS_HOME/bin/standalone.sh ${JVM_OPTS} -b 0.0.0.0 -bmanagement 0.0.0.0